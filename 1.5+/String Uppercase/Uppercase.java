/*
 * Esercizio.java
 * Created on 19-10-2010 05:05 PM
 *
 */

import java.util.Scanner;

public class Uppercase {
   
   /*
    *   PROCEDURA - MAIN
    * ------------------------------------
    *
    *   Input:      String[]
    *   Return:     Void
    *   Err.Chk:    True
    *
    */
   public static void main(String[] Args){
       
       String _strStorage = new String();
       Scanner _inHandle = new Scanner(System.in);
       int _strPosition = 0;
              
       System.out.print("\n - Immettere una stringa:  ");
       _strStorage = _inHandle.nextLine().toUpperCase();
       
       while(true){
           System.out.print(" - Posizione del carattere da visionare:  ");
           _strPosition = _inHandle.nextInt();
           
           if(_strStorage.length() >= _strPosition && 
              _strPosition > 0)
           {
               System.out.println("\n Stringa scelta:  " + _strStorage);
               System.out.println(" Carattere estratto:  " + _strStorage.charAt(_strPosition - 1));    
               break;
           }
           else System.out.println((_strPosition > 0) ?
                                    "\n Valore immesso superiore al numero di caratteri della stringa!\n" : 
                                    "\n Il valore immesso risulta negativo!\n");
       }
       
   }
}