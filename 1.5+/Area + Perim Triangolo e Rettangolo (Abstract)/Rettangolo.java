public class Rettangolo extends Poligono{

    private int base = 0;
    private int altezza = 0;
    
    public Rettangolo(int b, int h){
        super( 4 );
        
        base = b;
        altezza = h;
    }
    
    @Override
    public int calcArea(){
        return (base * altezza);
    }
    
    @Override
    public int calcPerim(){
        return (base + altezza)*2;
    }
    
}