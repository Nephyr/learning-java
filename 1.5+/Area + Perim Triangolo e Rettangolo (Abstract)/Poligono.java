public abstract class Poligono{
    
    private int numLati = 0;
    
    public Poligono(int lati){
        numLati = lati;
    }

    public abstract int calcArea();
    public abstract int calcPerim();
}