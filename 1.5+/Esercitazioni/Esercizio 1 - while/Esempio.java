/*
*	Scrivere un programma che legge una sequenza di numeri interi tramite l'apposito 
*	metodo della classe Scanner. Il programma deve contare quante volte, in questa sequenza, 
*	� contenuto il numero 10 e stampare questa informazione a video. 
*	Si supponga che la sequenza termini quando viene letto il numero 0.
*
*/

import java.util.Scanner;

public class Esempio{

	public static void main(String[] args){
	
		Scanner _handle = new Scanner(System.in);
		int _ipt = 0, counter = 0;
		
		System.out.print("Inserire un numero intero:  ");
		while((_ipt = _handle.nextInt()) != 0)
		{
			System.out.print("Inserire un numero intero:  ");
			
			if(_ipt == 10)
				counter++;
		}
		
		System.out.println("Il numero 10 e' stato digitato " + counter + " volt" + ((counter > 1) ? "e" : "a"));
	
	}

}