import java.util.Scanner;

public class EstrazioniDelLotto 
{
    public static void main(String[] args) {
        /**
         *  Dichiarazioni variabili
         */
        Scanner         inputHandle;
        int             vettNum[];
        short           indexNum;
        Ruota           ruotaController;
        Ruota           tmpStartCtrl;
        
        /**
         *  Inizializzazioni variabili
         */
        vettNum         = new int[5];
        inputHandle     = new Scanner(System.in);
        indexNum        = 0;
        ruotaController = new Ruota();
        tmpStartCtrl    = ruotaController;
        
        /**
         *  Ciclo Utenza / Programma
         */
        while( true )
        {
            indexNum = 0;
            System.out.println("\n Immetti cinque valori interi.\n");
            do {
                System.out.print(" - Inserisci il #" + (indexNum + 1) + " Numero: ");
                vettNum[indexNum] = inputHandle.nextInt();
            } while( ++indexNum < 5 );
            
            /**
             *  Uso vettNum.clone() altrimenti passo 
             *  l'indirizzo dello stesso vettore.
             *  Se non usassi clone() otterrei la
             *  stessa ruota ovunque!!
             */
            ruotaController.setNumeri( vettNum.clone() );
            
            System.out.print("\n - Citta' a cui assegnare la ruota?: ");
            ruotaController.setCitta( inputHandle.next() );
            
            ruotaController.setNext( new Ruota() );
            ruotaController = ruotaController.getNext();
            
            System.out.print(" - Continuare ad inserire ruote? [y = continua]: ");
            if(inputHandle.next().toLowerCase().charAt(0) != 'y')
                break;
        }
        
        /**
         *  Torno alla root
         */
        ruotaController = tmpStartCtrl;
        
        /**
         *  Ciclo di stampa
         */
        System.out.println("\n Le ruote da te immesse:\n");
        while(ruotaController.hasNext()){
            System.out.println(" - " + ruotaController.toString());
            ruotaController = ruotaController.getNext();
        }
    }
}
