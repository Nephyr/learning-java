REM *
REM * 	Batch Script: JCompile.bat, Rev 4 - Ver 1.1
REM * ----------------------------------------------------------------------
REM *
REM *	Produttori:			Marco Ricotta
REM *						Vanzo Luca Samuele
REM *
REM *	Copyright (c) 2010.
REM *
REM * ----------------------------------------------------------------------
REM *
REM * 	Compilatore:		Javac SDK
REM * 	Interprete:			Java VM
REM * 	Terminal:			MS-DOS / Altri ???
REM *
REM * 	Altro: 
REM *
REM *		- Input utente
REM *		- Notifiche a video
REM *		- Directory di compilazione a scelta
REM *
REM * ----------------------------------------------------------------------
@echo off
cls

REM * Titolo JCompiler.bat.
echo+
echo                     #                        ######               
echo                     #   ##   #    #   ##     #     #   ##   ##### 
echo                     #  #  #  #    #  #  #    #     #  #  #    #   
echo                     # #    # #    # #    #   ######  #    #   #   
echo               #     # ###### #    # ######   #     # ######   #   
echo               #     # #    #  #  #  #    #   #     # #    #   #   
echo                #####  #    #   ##   #    #   ######  #    #   #   
echo+
echo -------------------------------------------------------------------------------
echo             Copyright (C) 2010 - Marco Ricotta e Luca Samuele Vanzo
echo -------------------------------------------------------------------------------
echo+
echo+

REM * Input da tastiera con check NULL.
:input
set INPUT=
set /P INPUT=Inserire il nome file senza estensione: %=%
if "%INPUT%"=="" goto input
if NOT EXIST "%INPUT%.java" echo Il file indicato non esiste.
if NOT EXIST "%INPUT%.java" goto input

REM * Input da tastiera con check NULL.
:input_dir
set INPUT_DIR=
set /P INPUT_DIR=Inserire la destinazione di compilazione: %=%
if "%INPUT_DIR%"=="" goto input_dir

REM * Notifica pre-compilazione.
echo+
echo Il file %INPUT% verra' ora compilato in:  [CurrentDir]/%INPUT_DIR%.
echo Al termine il programma verr� eseguito automaticamente.

IF NOT EXIST "%INPUT_DIR%" mkdir %INPUT_DIR%
echo Attendere prego...
javac -classpath . %INPUT%.java -d %INPUT_DIR%

echo+
echo+
echo -------------------------------------------------------------------------------
echo+
echo   Interprete:       Java Virtual Machine
echo   Java/c Bat:       File: "%INPUT%.class" in Dir: "%INPUT_DIR%"
echo                     Running Code...
echo+
echo -------------------------------------------------------------------------------
echo             Copyright (C) 2010 - Marco Ricotta e Luca Samuele Vanzo
echo -------------------------------------------------------------------------------
echo+
echo+

cd %INPUT_DIR%
java %INPUT%

echo+
echo+
echo -------------------------------------------------------------------------------
echo Programma terminato. Premere un tasto per uscire...
pause > nul