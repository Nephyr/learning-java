import java.io.File;

public class Demo_FileDirTree {
    public static void main(String[] args) {
        String path = "c:\\windows";
        File f = new File(path);
        listRecursive(f);
    }

    private static void listRecursive(File f) {
        try{
            if (f.exists()){
                File[] el = f.listFiles();
                for (int i = 0; i < el.length; i++) {
                    if (el[i].isDirectory()) {
                        System.out.println("Directory: " + el[i]);
                        listRecursive(el[i]);
                    } else {
                        System.out.println("File: " + el[i]);
                    }
                }
            }
        }
        catch(Exception e)
        {
            System.out.println("File SKIP!");
        }
    }
}
