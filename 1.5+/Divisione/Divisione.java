/*
 *  Divisione.java
 *  Created on 15-10-2010 10:29 AM
 *
 *  NaviCoder IDE (c) Lite
 *
 */

public class Divisione {
   
   /*
    *   PROCEDURA - MAIN
    * -------------------------------------------
    *
    *   Return:     Void
    *   Input:      String[]
    *   Err.Chk:    True
    *
    */
    public static void main(String[] args){
        int x = 3, y = 5;
        try{
            System.out.println("\n Risultato divisione:  " + y/x);
            System.out.println(" Resto divisione:  " + y%x);
        }
        catch(Exception e){
            System.out.println(e);
            return;
        }
		
    }
   
}