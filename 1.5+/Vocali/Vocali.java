/*
 * Vocali.java
 *
 * Prova Java application
 * Created on 24-10-2010 05:35 PM
 */

import java.util.Scanner;

public class Vocali{
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        String voc = "aeiou";
        int counter = 0;
        
        System.out.print("\n - Immetti una stringa:  ");
        String str = in.nextLine();
        
        for(int i=0; i<str.length(); i++)
        {
            if(voc.indexOf(str.charAt(i)) != -1)
                counter++;
        }
        
        System.out.println(" - Numero di vocali:  " + counter);
    }
}