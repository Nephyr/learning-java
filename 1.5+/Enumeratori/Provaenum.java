public class Provaenum
{
    public enum GiorniSettimana { 
        LUNEDI, 
        MARTEDI, 
        MERCOLEDI, 
        GIOVEDI,
        VENERDI, 
        SABATO, 
        DOMENICA;
    }
    
    public enum OPCODE { 
        OP_ACCEPT( 0x0120 ),
        OP_READ( 0x0100 );
        
        private int hex;
        private OPCODE(int x) { hex = x; }
        public String getValueAsConstChar() { return "Valore OPCODE: " + hex /* toInteger */; }
    }

    public void getDayAsConstChar( GiorniSettimana giorno )
    {
        switch ( /*GiorniSettimana*/ giorno )
        {
            case LUNEDI:
                System.out.println("E' lunedi'.... eeeeee tutto va bene...");
                break;

            case DOMENICA:
                System.out.println("E' domenica.... eeeeeee dormo...");
                break;
            
            default:
                System.out.println("Yes.... my lord... e' un giorno qualunque...");
                break;
        }
    }
    
    public static void main(String[] arg){
        
        Provaenum p = new Provaenum();
        
        // Errore di compilazione
        GiorniSettimana Giorno = GiorniSettimana.TRENTORDICI;
        
        // Enum base
        p.getDayAsConstChar(GiorniSettimana.LUNEDI);
        p.getDayAsConstChar(GiorniSettimana.MARTEDI);
        p.getDayAsConstChar(GiorniSettimana.DOMENICA);
        
        // Enum con valore.
        System.out.println( OPCODE.OP_ACCEPT.getValueAsConstChar() );
        System.out.println( OPCODE.OP_ACCEPT );
    }
}