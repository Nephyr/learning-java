/*
 *  Calcolatore.java
 *  Created on 15-10-2010 10:29 AM
 *
 *  NaviCoder IDE (c) Lite
 *
 */

import java.util.Scanner;

public class Calcolatore {
   
   /*
    *   PROCEDURA - AREA TRAPEZIO
    * -------------------------------------------
    *   
    *   Ritorna il valore dell'area.
    *   Esegue check per errori di runtime.
    *   
    * -------------------------------------------
    *
    *   Return:     float
    *   Input:      float
    *   Err.Chk:    True
    *
    */
   public static float CalcTrapezio( float b1, float b2, float h ){
        try{
            return ((b1+b2)*h)/2;
        }
        catch(Exception e){
            System.out.println(e);
            return 0;
        }
   }
   
   /*
    *   PROCEDURA - AREA ROMBO
    * -------------------------------------------
    *   
    *   Ritorna il valore dell'area.
    *   Esegue check per errori di runtime.
    *   
    * -------------------------------------------
    *
    *   Return:     float
    *   Input:      float
    *   Err.Chk:    True
    *
    */
   public static float CalcRombo( float d1, float d2 ){
        try{
            return (d1*d2)/2;
        }
        catch(Exception e){
            System.out.println(e);
            return 0;
        }
   }
   
   /*
    *   PROCEDURA - AREA PARALLELOGRAMMA
    * -------------------------------------------
    *   
    *   Ritorna il valore dell'area.
    *   Esegue check per errori di runtime.
    *   
    * -------------------------------------------
    *
    *   Return:     float
    *   Input:      float
    *   Err.Chk:    True
    *
    */
   public static float CalcParal( float b, float h ){
        try{
            return b*h;
        }
        catch(Exception e){
            System.out.println(e);
            return 0;
        }
   }
   
   /*
    *   PROCEDURA - MAIN
    * -------------------------------------------
    *
    *   Return:     Void
    *   Input:      String[]
    *   Err.Chk:    True
    *
    */
    public static void main(String[] args){
        Scanner inHandle = new Scanner(System.in);
        byte ch = 0;
        float tB1, tB2, tH, tD1, tD2;
                        
        while(true){
            try{
                System.out.println("\n Quale tipo di calcolo si vuole effettuare?\n");
                System.out.println("\t 0. Chiudi Programma.");
                System.out.println("\t 1. Area Trapezio.");
                System.out.println("\t 2. Area Rombo.");
                System.out.println("\t 3. Area Parallelogramma.");
                System.out.print("\n Digitare un numero [0 a 3]:  ");
                
                ch = inHandle.nextByte();
                
                switch(ch)
                {
                    case 0:
                        return;
                    
                    case 1: 
                        tB1 = tB2 = tH = 0;
                        
                        System.out.println("\n Calcolo area del Trapezio.");
                        System.out.print(" Digitare un valore per la base minore:  ");
                        tB1 = inHandle.nextFloat();
                        System.out.print(" Digitare un valore per la base maggiore:  ");
                        tB2 = inHandle.nextFloat();
                        System.out.print(" Digitare un valore per l'altezza:  ");
                        tH = inHandle.nextFloat();
                        
                        System.out.print("\n Area risultante:  " + CalcTrapezio(tB1, tB2, tH) + "\n");
                        break;
                        
                    case 2:
                        tD1 = tD2 = 0;
                        
                        System.out.println("\n Calcolo area del Rombo.");
                        System.out.print(" Digitare un valore per la diagonale minore:  ");
                        tD1 = inHandle.nextFloat();
                        System.out.print(" Digitare un valore per la diagonale maggiore:  ");
                        tD2 = inHandle.nextFloat();
                        
                        System.out.print("\n Area risultante:  " + CalcRombo(tD1, tD2) + "\n");
                        break;
                        
                    case 3:
                        tB1 = tH = 0;
                        
                        System.out.println("\n Calcolo area del Parallelogramma.");
                        System.out.print(" Digitare un valore per la base:  ");
                        tB1 = inHandle.nextFloat();
                        System.out.print(" Digitare un valore per l'altezza:  ");
                        tH = inHandle.nextFloat();
                        
                        System.out.print("\n Area risultante:  " + CalcParal(tB1, tH) + "\n");
                        break;
                    
                    default:
                        break;
                }
            }
            catch(Exception e){
                System.out.println(e);
                return;
            }
        }
    }
   
}