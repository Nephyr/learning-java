//======================================================================================
//  FILE: MyFileReader.java
//======================================================================================

import java.io.*;
import java.util.*;

public class MyFileReader
{
    // Path dei files
    private String _pathin;
    private String _pathout;

    public MyFileReader(String _in, String _out)
    {
        _pathin = _in;
        _pathout = _out;
    }
    
    public byte GetData(int _min, int _max)
    {
        // Dichiarazioni
        Vector<String> _storage = new Vector<String>();
        String _buffer = "";
        int _distance = 0;
        int _token = 0;
        
        try {
            // Apro i canali di stream
            FileReader _fr = new FileReader(_pathin);
            FileWriter _fw = new FileWriter(_pathout);
            
            while(_distance > -1)
            {
                // Leggo il token dallo stream
                _token = _fr.read();
            
                // Controllo l'esistenza di una parola
                if((char)_token == ' ' || _token == -1)
                {
                    // Se distanza > 0 allora � una parola
                    if(_distance > 0)
                    {
                        // Pulisco la parola
                        _buffer = _buffer.replaceAll("\r", "");     // Carriage Return
                        _buffer = _buffer.replaceAll("\n", "");     // New Line
                        _buffer = _buffer.replaceAll("\t", "");     // Tabulation
                        _buffer = _buffer.trim();
                        
                        // Salvo la parola in _storage 
                        if(_buffer.length() >= _min && _buffer.length() <= _max)
                            _storage.add(_buffer);
                        
                        // Resetto il buffer
                        _buffer = "";
                    }
                    
                    // Reset della distanza
                    // oppure esco dalla lettura
                    if(_token == -1) _distance = -1;
                    else _distance = 0;
                }
                else
                {
                    // Incremento la distanza
                    // e salvo nel buffer
                    ++_distance;
                    _buffer += (char)_token;
                }
            }
            
            // Estraggo la stringa da scrivere
            for(String _str : _storage)
            {
                // Estraggo i token dalla stringa
                for(char c : _str.toCharArray())
                {
                    // Scrivo nel canale stream
                    _fw.write(c);
                }
                
                // Vado a capo... 
                // Commento inutile XD
                _fw.write("\r\n");
            }
            
            // Chiudo il canale stream
            _fw.close();
            _fr.close();
            
            // Scrittura non vuota
            // Return: status
            if(_storage.size() > 0)
                return 2;
        }
        // Controllo eccezioni
        catch(IOException e)
        {
            // Return: status
            return 1;
        }
        
        // Return: status
        return 0;
    }

    public static void main(String[] args)
    {
        // Dichiarazione gestore input
        Scanner _input = new Scanner(System.in);
        
        //
        //  INPUT: FILES DI LETTURA/SCRITTURA
        //
        System.out.print("\n --> Path assoluta file da leggere: ");
        String _pathin = _input.next();
        //
        System.out.print(" --> Path assoluta file da scrivere: ");
        String _pathout = _input.next();
        
        //
        //  INPUT: RANGE DIMENSIONE PAROLE
        //
        System.out.print(" --> Dimensione minima parola: ");
        int _min = _input.nextInt();
        //
        System.out.print(" --> Dimensione massima parola: ");
        int _max = _input.nextInt();
        
        //
        //  EXEC: LETTURA E SCRITTURA DELLE PAROLE
        //
        MyFileReader b = new MyFileReader(_pathin, _pathout);
        byte _res = b.GetData(_min, _max);
        
        //
        //  OUTPUT: STATO DELLA LETTURA AL TERMINE
        //
        switch(_res)
        {
            case 0:
                System.out.println("\n Lettura completata con successo.\n" + 
                                   " Nessuna parola trovata compresa tra: " + _min + " e " + _max);
                break;
                
            case 1:
                System.out.println("\n ERRORE: Eccezione di input/output!");
                break;
                
            case 2:
                System.out.println("\n Lettura completata con successo.\n" + 
                                   " Parole trovate salvate in: " + _pathout);
                break;
        }
    }
}

//======================================================================================
//  FILE: MyFileReaderException.java
//======================================================================================

class MyFileReaderException extends Exception
{
    private String _msg;

    public MyFileReaderException()
    {
        super();
        _msg = "Unknown";
    }
    
    public MyFileReaderException(String _err)
    {
        super();
        _msg = _err;
    }
    
    public String Message()
    {
        return _msg;
    }
}