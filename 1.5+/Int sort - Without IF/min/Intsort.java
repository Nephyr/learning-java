/*
 * Intsort.java
 * Created on 21-10-2010 11:18 AM
 *
 */

import java.util.*;

public class Intsort {
   
   /*
    *   PROCEDURA - MAIN
    * -------------------------------------
    *
    *   Return:     Void
    *   Input:      String[]
    *   Err.Chk:    True
    *
    */
   public static void main(String[] Args) {
       
       try{
            int x[] = new int[3];
            Scanner s = new Scanner(System.in);
            
            for(short i=0; i<x.length; i++)
            {
                System.out.print(" - Immettere il valore intero umero " + (i+1) + ":  ");
                x[i] = s.nextInt();
            }
            
            Arrays.sort(x);
            System.out.println("\n - Il minimo e':  " + x[0]);
       }
       catch(Exception e)
       {
           System.out.println(e);
       }
   }
   
}