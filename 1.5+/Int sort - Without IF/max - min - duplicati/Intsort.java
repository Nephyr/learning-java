/*
* Intsort.Java
* Created on 21-10-2010 11:18 AM
*
*/

import java.util.*;

public class Intsort {

    /*
    *   PROCEDURA - MAIN
    * -------------------------------------
    *
    *   Return:     Void
    *   Input:      String[]
    *   Err.Chk:    True
    *
    */
    public static void main(String[] Args) {

        try{
            // Inizializzazione variabili
            int x[] = new int[10];
            int counter = 0;
            Scanner inputHandler = new Scanner(System.in);
            boolean inChk = false;

            System.out.print("\n");
            for(int i=0; i<x.length; i++)
            {
                // Controllo di validit�
                inChk = false;
                
                do
                {
                    // FIX: Riduzione carico CPU
                    Thread.currentThread().sleep(1);

                    // Stampo avviso
                    System.out.print(" - Immettere il valore intero umero " + (i+1) + ":  ");

                    // Controllo tipo di input
                    if(inputHandler.hasNextInt())
                    {
                        // Se valido assegno a int
                        x[i] = inputHandler.nextInt();
                        inChk = true;
                    }
                    else if(inputHandler.hasNextLine())
                    {
                        // Flush input a vuoto
                        inputHandler.nextLine();
                        inChk = false;
                    }
                    else
                    {
                        // Flush input a vuoto
                        inputHandler.next();
                        inChk = false;
                    }

                    // Loop finch� dato invalido invalido
                } while(!inChk);
            }

            // Ordino il vettore [0] = minimo, [length - 1] = massimo
            Arrays.sort(x);
            
            for(int i=0; i<x.length; i++)
                if((i > 0) ? x[i] != x[i-1] : x[i] != x[i+1])
                    for(int n=i; n<x.length; n++)
                    {
                        if(x[i] == x[n])
                            counter++;
                        
                        break;
                    }

            // Stampo i risultati
            System.out.println("\n -----------------------------------------");
            System.out.println("\n - Il massimo immesso:  " + x[x.length - 1]);
            System.out.println(" - Il minimo immesso:  " + x[0]);
            System.out.println(" - Valori duplicati:  " + counter);
        }
        catch(Exception e)
        {
            // Anti crash + Dump exception
            System.out.println(e);
        }
    }
}