import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class WindowClassGUI extends JFrame{
    private JTextField text;
    private JButton btn;
    private JPanel panel;
    
    public WindowClassGUI(){
        super("Applicativo a Finestra");
        text = new JTextField();
        panel = new JPanel();
        panel.setLayout(new GridLayout(2,1)); // tabella 2 righe e 1 colonna
        btn = new JButton("Bottone di Prova");
        panel.add(btn);
        panel.add(text);
        this.add(panel);
        this.pack();
        this.setVisible(true);
    }
}

public class WindowedForm{
    public static void main(String[] args){
        javax.swing.SwingUtilities.invokeLater( new Runnable(){
            public void run(){
                WindowClassGUI gui = new WindowClassGUI();
            }
        });
    }
}