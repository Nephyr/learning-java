public class Singlecharread{

	public static void main(String[] Args){
		
		try{
			char c1, c2;
			
			/*
			 *	INPUT - CARATTERE 1
			 *
			 */
			System.out.print("\n - Immettere il primo carattere:  ");
			c1 = (char)System.in.read();
			
			/*
			 *	FLUSH INPUT E BUFFER
			 * -----------------------------
			 *
			 *	Elimino dal buffer tutti gli n 
			 *	caratteri dove n = available()
			 *
			 */
			System.in.skip(System.in.available());
			
			/*
			 *	INPUT - CARATTERE 2
			 *
			 */
			System.out.print(" - Immettere il secondo carattere:  ");
			c2 = (char)System.in.read();
			
			/*
			 *	CONTROLLO CARATTERI IMMESSI
			 *
			 */
			if(c1 == c2)
				System.out.println("\n I caratteri immessi sono uguali!!");
			else
				System.out.println("\n I caratteri immessi NON sono uguali!!");
		}
		catch(Exception e)
		{
			/*
			 *	ERR. RUNTIME
			 * -----------------------------
			 *
			 *	La (char)System.in.read()
			 *	richiede la gestione degli errori
			 *
			 */
			e.printStackTrace();
		}
	}

}