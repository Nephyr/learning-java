import java.util.ArrayList;
import java.util.Scanner;

public class NotifyWait
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		Storage str = new Storage();
		Worker[] wrk = new Worker[4];
		
		for(int i = 0; i < 4; i++)
		{
			wrk[i] = new Worker(i, str);
			wrk[i].start();
		}
		
		while (true)
		{
			String cmd = input.nextLine();
			
			if(cmd.equals("exit"))
				break;
			
			str.addJobs(cmd);
		}
	}

}

class Storage
{
	private ArrayList<String> jobs;
	
	public Storage() 
	{ 
		jobs = new ArrayList<String>();
	}

	public synchronized void addJobs(String txt) 
	{ 
		jobs.add(txt);
		notify();
	}
	
	public synchronized boolean Empty(int idx) throws InterruptedException 
	{ 
		if(jobs.isEmpty())
		{
			System.out.println( "Thread[" + idx + "]->Wait()" );
			wait();
		}
		
		return jobs.isEmpty();
	}
	
	public synchronized String getJob()
	{
		if (jobs.isEmpty())
			return null;
		
		String tmp = jobs.get(0);
		jobs.remove(0);
		
		return tmp;
	}
}

class Worker extends Thread
{
	private int id;
	private Storage j;
	
	public Worker(int n, Storage obj) 
	{ 
		id = n;
		j = obj;
	}
	
	public void run()
	{
		try{
			while (true)
			{
				while(j.Empty(id)) {}
				
				System.out.println( "Thread[" + id + "]->Println():   " + j.getJob() );
			}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}